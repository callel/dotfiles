#!/bin/bash

set -u

read -r -d '' layouts <<-EOF
MainAndVertStack
MainAndHorizontalStack
MainAndDeck
GridHorizontal
EvenHorizontal
EvenVertical
Fibonacci
LeftMain
CenterMain
CenterMainBalanced
CenterMainFluid
Monocle
RightWiderLeftStack
LeftWiderRightStack
EOF

selected_layout=$( echo "$layouts" | rofi -dmenu -i -auto-select -p "Layout:" )

leftwm-command "SetLayout $selected_layout"
