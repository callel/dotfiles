#! /bin/sh

find $HOME/wallpapers/ -type f | shuf -n 1 | xargs xwallpaper --zoom
