set fish_greeting
set -gx TERM "xterm-kitty"
set -gx VISUAL "emacsclient -c"
set -gx EDITOR "$VISUAL"
set -gx GOPATH "$HOME/go"
set -gx ROLINKS "$HOME/documents/links"

fish_add_path $GOPATH/bin
fish_add_path $HOME/.local/bin
fish_add_path $HOME/.cargo/bin
fish_add_path $HOME/zig

## ALIASES ##

# navigation
alias ..='cd ..'
alias ...='cd ../..'
alias .3='cd ../../..'
alias .4='cd ../../../..'
alias .5='cd ../../../../..'

# exa as ls
alias ls='exa -al --color=always --group-directories-first'
alias lt='exa -aT --color=always --group-directories-first'

alias hx='helix'

zoxide init fish | source
starship init fish | source
