;;; init.el -*- lexical-binding: t; -*-

;; Add the modules folder to the load path
(add-to-list 'load-path (expand-file-name "modules/" user-emacs-directory))

;; Set default coding system (especially for Windows)
(set-default-coding-systems 'utf-8)
(customize-set-variable 'visible-bell 1)  ; turn off beeps, make them flash!
(customize-set-variable 'large-file-warning-threshold 100000000) ;; change to ~100 MB

(require 'cal-no-cluttering)      
(require 'cal-setup)
(require 'cal-defaults)
(require 'cal-editing)
(require 'cal-meow)
(require 'cal-completions)

(defun cal/hide-line-numbers ()
  (display-line-numbers-mode 0))

(setup display
  (set-face-attribute 'default nil :font "JetBrainsMono Nerd Font")

  (:with-mode prog-mode
    (:hook show-paren-mode)))

(setup emacs
  (setq-default tab-width 2
                tab-always-indent 'complete)
  (:global "C-M-y" #'clipboard-yank))

(setup line-numbers
  (column-number-mode)
  (global-display-line-numbers-mode t)
  (:with-mode (org-mode
               term-mode
               shell-mode
               eshell-mode)
    (:hook cal/hide-line-numbers)))

(setup (:pkg doom-themes)
  (load-theme 'doom-one t)
  (:when-loaded
    (setq doom-themes-enable-bold t
          doom-themes-enable-italic t)
    
    (doom-themes-visual-bell-config)
    (doom-themes-org-config)))

(setup
    (:pkg doom-modeline)
  (doom-modeline-mode 1))

(setup (:pkg crux)
  (:global [remap move-beginning-of-line] #'crux-move-beginning-of-line
           "C-<return>" #'crux-smart-open-line
           "C-M-<return>" #'crux-smart-open-line-above
           [remap kill-line] #'crux-smart-kill-line
           "C-c d" #'crux-duplicate-current-line-or-region))

(setup (:pkg which-key)
  (which-key-mode)
  (:when-loaded (setq which-key-idle-delay 0.3)))

(setup (:pkg avy)
  (:global "C-ö" avy-goto-char-timer
           "C-c C-ö l" avy-copy-line))

(defun cal/org-mode-setup ()
  (org-indent-mode)
  (variable-pitch-mode 1)
  (visual-line-mode 1))

(setup org
  (:hook cal/org-mode-setup)
  (:when-loaded
    (setq org-hide-emphasis-markers t)))

(setup (:pkg (parinfer-rust-mode
               :type git
               :host github
               :repo "justinbarclay/parinfer-rust-mode"))
  (:option parinfer-rust-preferred-mode "indent")
  (:hook-into emacs-lisp-mode) 
  (:hook-into scheme-mode)
  (:hook-into lisp-mode))

(setup (:pkg go-mode)
  (:local-hook before-save-hook gofmt-before-save))

(setup (:pkg v-mode)
  (:bind "C-c C-v" v-menu))

(setup (:pkg zig-mode))

(setup (:pkg rust-mode))

(setup (:pkg haskell-mode))

(setup (:pkg (odin-mode :type git :host github :repo "mattt-b/odin-mode")))

(setup (:pkg (nim-mode)))


(setup (:pkg chezmoi))

;; Make GC pauses faster by decreasing the threshold.
(setq gc-cons-threshold (* 2 1000 1000))

(custom-set-variables
 ;; custom-set-variables was added by Custom.
 ;; If you edit it by hand, you could mess it up, so be careful.
 ;; Your init file should contain only one such instance.
 ;; If there is more than one, they won't work right.
 '(custom-safe-themes
   '("835868dcd17131ba8b9619d14c67c127aa18b90a82438c8613586331129dda63" default)))
(custom-set-faces)
 ;; custom-set-faces was added by Custom.
 ;; If you edit it by hand, you could mess it up, so be careful.
 ;; Your init file should contain only one such instance.
 ;; If there is more than one, they won't work right.
