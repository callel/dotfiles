;;; cal-editing.el -*- lexical-binding: t; -*-

(delete-selection-mode 1)

(electric-pair-mode 1)

(global-set-key (kbd "M-u") 'upcase-dwim)
(global-set-key (kbd "M-l") 'downcase-dwim)
(global-set-key (kbd "M-c") 'capitalize-dwim)

(provide 'cal-editing)
