#!/usr/bin/env nu

def main_menu [links] {
  do -i {
    $links
    | each { |l| format_link $l }
    | str collect '|'
    | rofi -dmenu -i -sep "|" -eh 2 -markup-rows -format i -p bmarks -kb-custom-1 "Alt-n" -mesg "dep: <span weight='bold'>Alt-n</span> New link"
  } | complete
}

def parse_links [] {
  open ~/documents/links
  | lines
  | each { |l|
      let i = ($l | str index-of ' ')
      let url = ($l | str substring [0 $i])
      let cleanUrl = ($url | str find-replace 'http(s)?://(www\.)?' '')
      let tagStr = ($l | str substring $"($i + 1),")
      let tags = ($tagStr | split row ';' | each {|t| $t | str trim })
      { url: $url, cleanUrl: $cleanUrl, tags: $tags}
    }
}

def format_link [link] {
  let tags = ($link.tags | str collect ' • ')
  $'<span size='x-small' color='gray'>($tags)</span>(char nl)($link.cleanUrl)'
}

def run_result [result links] {
  if $result.exit_code == 0 {
    let selection = ($result.stdout | into decimal | into int)
    let link = ($links | get $selection)
  
    $link
    | get url
    | xclip -rmlastnl -selection clipboard
  } else if $result.exit_code == 10 {
    new_entry_menu
  }
}

def new_entry_menu [] {
  let initUrl = xclip -o -selection clipboard
  let result = (
    rofi -dmenu -p url -filter $initUrl
    | complete
  )
  
  let url = ($result.stdout | str trim)
  if $result.exit_code == 0 && $url != '' {
    print $url
  } else {
    print 'cancel'
  }
}

let links = parse_links
let result = main_menu $links
run_result $result $links
