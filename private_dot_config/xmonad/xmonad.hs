import XMonad
import System.Exit
import qualified XMonad.StackSet as W

import XMonad.Hooks.ManageDocks (avoidStruts, docks)
import XMonad.Hooks.EwmhDesktops (ewmh, ewmhFullscreen)
import XMonad.Hooks.DynamicLog

import XMonad.Layout.Spacing
import XMonad.Layout.Renamed
import XMonad.Layout.ResizableTile

import XMonad.Util.EZConfig (mkKeymap, checkKeymap)
import XMonad.Util.Run (spawnPipe)
import XMonad.Util.SpawnOnce

import qualified DBus as D
import qualified DBus.Client as D

import qualified Codec.Binary.UTF8.String as UTF8

myTerminal = "kitty"

myBorderWidth = 2

myModMask = mod4Mask

myNormalBorderColor = "#4c566a"
myFocusedBorderColor = "#5e81ac"

myWorkspaces =
    ["1", "2", "3", "4", "5", "6", "7", "8", "9", "0"]

myKeys conf =
    [ ("M-C-r", spawn "xmonad --recompile; xmonad --restart")
    , ("M-M1-q", io (exitWith ExitSuccess))

    , ("M-<Return>", spawn $ myTerminal ++ " -e fish")
    , ("M-p", spawn "rofi -show drun")
    , ("M-S-p", spawn "exe=`dmenu_path | dmenu` && eval \"exec $exe\"")
    , ("M-b", spawn "$HOME/.config/xmonad/scripts/bookmarks.sh")

    , ("M-x", kill)

    , ("M-<Space>", sendMessage NextLayout)
    , ("M-S-<Space>", setLayout $ XMonad.layoutHook conf)
    , ("M-n", refresh)
    
    , ("M-S-m", windows W.swapMaster)
    , ("M-m", windows W.focusMaster)
    , ("M-<Tab>", windows W.focusDown)
    , ("M-k", windows W.focusDown)
    , ("M-i", windows W.focusUp)
    , ("M-S-k", windows W.swapDown)
    , ("M-S-i", windows W.swapUp)
    , ("M-j", sendMessage Shrink)
    , ("M-l", sendMessage Expand)
    , ("M-M1-k", sendMessage MirrorShrink)
    , ("M-M1-i", sendMessage MirrorExpand)
    , ("M-t", withFocused $ windows . W.sink)
    , ("M-,", sendMessage (IncMasterN 1))
    , ("M-.", sendMessage (IncMasterN (-1)))
    , ("<XF86AudioLowerVolume>", spawn "pamixer -d 5")
    , ("<XF86AudioRaiseVolume>", spawn "pamixer -i 5")
    ]
    ++ [("M-"++ m ++ k, windows $ f i)
            | (i, k) <- zip (XMonad.workspaces conf) myWorkspaces
            , (f, m) <- [(W.greedyView, ""), (W.shift, "S-")]]

myLayout = avoidStruts (tiled ||| Mirror tiled ||| Full)
  where
    -- default tiling algorithm
    tiled = renamed [Replace "Main"] $ spacing 2 $ ResizableTall nmaster delta ratio []
    -- default number of windows in master pane
    nmaster = 1
    -- defualt ratio occupied by master
    ratio = 1/2
    -- percent of screen to increment by when resizing panes
    delta = 3/100

myStartupHook = do
    -- checkKeymap myConfig (myKeys myLayout)
    spawnOnce "$HOME/.config/xrandr/run.sh"
    spawnOnce "$HOME/.config/calos/wallpaper.sh"
    spawnOnce "picom &"
    spawnOnce "numlockx &"
    spawn "$HOME/.config/polybar/launch.sh"
    return ()

myLogHook dbus = def
    { ppOutput = dbusOutput dbus
    , ppCurrent = wrap "%{F#d8dee9} " "%{F-}"
    , ppVisible= wrap "%{F#5e81ac} " "%{F-}"
    , ppUrgent= wrap "%{F#bf616a} " "%{F-}"
    , ppHidden= wrap "%{F#434c5e} " "%{F-}"
    , ppWsSep = ""
    , ppSep = " | "
    , ppTitle = shorten 75
    }

dbusOutput :: D.Client -> String -> IO ()
dbusOutput dbus str = do
    let signal = (D.signal objectPath interfaceName memberName) {
            D.signalBody = [D.toVariant $ UTF8.decodeString str]
        }
    D.emit dbus signal
    where
        objectPath = D.objectPath_ "/org/xmonad/Log"
        interfaceName = D.interfaceName_ "org.xmonad.Log"
        memberName = D.memberName_ "Update"

myConfig =
    def
      { modMask = myModMask
      , terminal = myTerminal
      , borderWidth = myBorderWidth
      , normalBorderColor = myNormalBorderColor
      , focusedBorderColor = myFocusedBorderColor
      , layoutHook = myLayout
      , startupHook = myStartupHook
      , keys = \c -> mkKeymap c (myKeys c)
      , workspaces = myWorkspaces
      }

main = do
    dbus <- D.connectSession
    D.requestName dbus (D.busName_ "org.xmonad.Log")
        [D.nameAllowReplacement, D.nameReplaceExisting, D.nameDoNotQueue]
        
    xmonad
        $ docks
        $ ewmhFullscreen
        $ ewmh
        $ myConfig { logHook = dynamicLogWithPP (myLogHook dbus) }
