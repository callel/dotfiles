#!/usr/bin/env bash

mullvad_status(){
    mullvad status | cut -d' ' -f3
}

print_icon(){
    echo "%{T6}%{F$1} %{T- F-}"
}

print_status(){
    case "$(mullvad_status)" in
        "Connected")
            print_icon '#a3be8c'
            ;;
        "Connecting")
            print_icon '#ebcb8b'
            ;;
        *)
            print_icon '#434c5e'
            ;;
    esac

}

mullvad_toggle(){
    if [[ "$(mullvad_status)" =~ Connect.* ]]; then
        mullvad disconnect
    else
        mullvad connect
    fi
}
case "$1" in
    --toggle)
        mullvad_toggle
        ;;
    *)
        print_status
        ;;
esac
