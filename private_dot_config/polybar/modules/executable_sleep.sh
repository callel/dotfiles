#!/usr/bin/env bash

sleep_status(){
    xset q | awk '/DPMS is / {print $3}'
}

print_icon(){
    echo "%{T3}%{F#81A1C1}$1 %{T- F-}"
}

print_status(){
    case "$(sleep_status)" in
        "Enabled")
            print_icon '鈴'
            ;;
        *)
            print_icon '零'
            ;;
    esac

}

sleep_toggle(){
    if [[ "$(sleep_status)" =~ Enabled ]]; then
        xset s off
        xset -dpms
    else
        xset s on
        xset +dpms
    fi
}

case "$1" in
    --toggle)
        sleep_toggle
        ;;
    *)
        print_status
        ;;
esac
