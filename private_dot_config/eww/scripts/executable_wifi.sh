#! /usr/bin/env bash

if [ $(nmcli g | grep -oE "disconnected") ]; then
  echo "./images/icons/network-disconnected.png"
else
  if [ $(mullvad status | grep -oE "Connected") ]; then
    echo "./images/icons/network-secure.png"
  else
    echo "./images/icons/network-connected.png"
  fi
fi
