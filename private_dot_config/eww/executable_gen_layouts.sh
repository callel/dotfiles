#! /usr/bin/env bash

ls -1 --color=never ./images/layouts/ | 
awk 'BEGIN {printf "$layouts: (" } END { printf ");" } // { printf $1 " " }' FS=. > ./layouts.scss